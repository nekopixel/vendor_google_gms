#!/bin/bash

if [ -z $VENDOR_GMS_SETUP_DONE ]; then

cd vendor/google/gms

git lfs pull

cd ../../../

export VENDOR_GMS_SETUP_DONE=true
fi
