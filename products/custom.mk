# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
    vendor/google/gms/products/overlay

PRODUCT_PACKAGE_OVERLAYS += \
    vendor/google/gms/products/overlay/common

# GMS RRO overlay
PRODUCT_PACKAGES += \
    PixelConfigOverlayCustom \
    SettingsGoogleOverlayCustom \
    SystemUIGoogleOverlayCustom \
    DocumentsUIGoogleOverlayCustom \
    PixelLauncherOverlayCustom

# Launcher overlay
ifeq ($(TARGET_DEVICE_IS_TABLET),true)
PRODUCT_PACKAGES += \
    NexusLauncherTabletOverlay
endif

# Custom Google apps whitelist
PRODUCT_PACKAGES += \
    custom-google-framework-sysconfig.xml \
    custom-google-hiddenapi-package-whitelist.xml \
    custom-google-platform.xml \
    custom-google-power-whitelist

# Custom GMS init script
PRODUCT_PACKAGES += \
    init.gms.rc

# Themed icons for Pixel Launcher
$(call inherit-product, packages/overlays/ThemeIcons/config.mk)
