#!/bin/bash
#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017-2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

set -e

DEVICE=common
VENDOR=google/gms

# Load extract_utils and do some sanity checks
MY_DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$MY_DIR" ]]; then MY_DIR="$PWD"; fi

ANDROID_ROOT="${MY_DIR}/../../.."

HELPER="${ANDROID_ROOT}/tools/extract-utils/extract_utils.sh"
if [ ! -f "${HELPER}" ]; then
    echo "Unable to find helper script at ${HELPER}"
    exit 1
fi
source "${HELPER}"

# Default to sanitizing the vendor folder before extraction
CLEAN_VENDOR=true

KANG=
SECTION=

while [ "${#}" -gt 0 ]; do
    case "${1}" in
        -n | --no-cleanup )
                CLEAN_VENDOR=false
                ;;
        -k | --kang )
                KANG="--kang"
                ;;
        -s | --section )
                SECTION="${2}"; shift
                CLEAN_VENDOR=false
                ;;
        * )
                SRC="${1}"
                ;;
    esac
    shift
done

if [ -z "${SRC}" ]; then
    SRC="adb"
fi

function blob_fixup() {
    case "${1}" in
        product/overlay/*apk)
            starletMagic $1 $2 &
            ;;
        product/etc/permissions/privapp-permissions-google-p.xml)
            sed -i '/com.google.android.googlequicksearchbox">/a\        <permission name="android.permission.RECORD_AUDIO"/>' $2 &
            ;;
        product/media/bootanimation-dark.zip)
            folder=${2/.zip/}
            filename=$(basename $1)
            unzip $2 -d $folder
            rm -rf $2
            sed -i 's/1080 2338 60/1080 2338 60 1/g' $folder/desc.txt
            (cd $folder && zip -r -0 ../$filename *)
            rm -rf $folder
            ;;
        vendor/etc/selinux/vendor_mac_permissions.xml)
            googlePlfSigUpdater $2
            ;;
    esac
}

function starletMagic() {
    folder=${2/.apk/}

    echo "    "${folder##*/} "\\" >> "${MY_DIR}/${DEVICE}/overlays.mk"

    apktool -q d "$2" -o $folder -f
    rm -rf $2 $folder/{apktool.yml,original,res/values/public.xml,unknown}

    cp ${MY_DIR}/overlay-template.txt $folder/Android.bp
    sed -i "s|dummy|${folder##*/}|g" $folder/Android.bp

    find $folder -type f -name AndroidManifest.xml \
        -exec sed -i "s|extractNativeLibs\=\"false\"|extractNativeLibs\=\"true\"|g" {} \;

    for file in $(find $folder/res -name *xml \
            ! -path "$folder/res/raw" \
            ! -path "$folder/res/drawable*" \
            ! -path "$folder/res/xml"); do
        for tag in $(cat exclude-tag.txt); do
            type=$(echo $tag | cut -d: -f1)
            node=$(echo $tag | cut -d: -f2)
            xmlstarlet ed -L -d "/resources/$type[@name="\'$node\'"]" $file
            xmlstarlet fo -s 4 $file > $file.bak
            mv $file.bak $file
        done
        sed -i "s|\?android:\^attr-private|\@\*android\:attr|g" $file
        sed -i "s|\@android\:color|\@\*android\:color|g" $file
        sed -i "s|\^attr-private|attr|g" $file
    done

    if [ "${folder##*/}" == "SettingsGoogleOverlay2021AndNewer" ]; then
        sed -i "s| android\:resourcesMap\=\"\@xml\/overlays\"||g" "$folder/AndroidManifest.xml"
        rm -rf "$folder/res/xml"
    fi
}

function googlePlfSigUpdater() {
    google_signature_config="${MY_DIR}/products/overlay/common/frameworks/base/core/res/custom_res/res/values/google_signature_config.xml"

    xmlstarlet ed -L \
        -d "/resources/string-array[@name='config_googlePlatformSignatures']/item" \
        $google_signature_config

    while IFS= read -r sigs; do
        xmlstarlet ed -L \
            -s "/resources/string-array[@name='config_googlePlatformSignatures']" \
            -t elem -n "item" -v $sigs \
            $google_signature_config
    done < <(xmlstarlet sel \
                -t -v "//policy/signer[seinfo/@value='platform']/@signature" -n \
                "$1")

    xmlstarlet fo -s 4 $google_signature_config > $google_signature_config.bak
    mv $google_signature_config.bak $google_signature_config

    rm $1
}

if [ -z "$SRC" ]; then
    echo "Path to system dump not specified! Specify one with --path"
    exit 1
fi

# Initialize the helper
setup_vendor "${DEVICE}" "${VENDOR}" "${ANDROID_ROOT}" false "${CLEAN_VENDOR}"

echo "PRODUCT_PACKAGES += \\" > "${MY_DIR}/${DEVICE}/overlays.mk"

extract "${MY_DIR}/proprietary-files.txt" "${SRC}" "${KANG}" --section "${SECTION}"
extract "${MY_DIR}/proprietary-files-gmscore.txt" "${SRC}" "${KANG}" --section "${SECTION}"
extract "${MY_DIR}/proprietary-files-dexopt.txt" "${SRC}" "${KANG}" --section "${SECTION}"

"${MY_DIR}/setup-makefiles.sh"

echo "Waiting for extraction"
wait
echo "All done"
